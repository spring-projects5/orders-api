package com.classpath.ordersapi.controller;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.model.OrderLineItem;
import com.classpath.ordersapi.model.Product;
import com.classpath.ordersapi.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Set;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/v1/orders")
@AllArgsConstructor
public class OrderController {
    private OrderService orderService;
    @PostMapping
    @ResponseStatus(CREATED)
    public Order saveOrder(@RequestBody Order order){
        /*order = new Order();

        OrderLineItem lineItem = new OrderLineItem();
        lineItem.setPrice(25000);
        lineItem.setQty(2);

        order.addLineItem(lineItem);
        order.setOrderDate(LocalDate.now());
        order.setCustomerName("Praveen");
        */
        //set the bidirectional mappling for all the order line items
        order.getOrderLineItems().forEach(lineItem -> lineItem.setOrder(order));
        return this.orderService.save(order);
    }

    @GetMapping
    public Set<Order> fetchOrders(){
        return this.orderService.fetchAll();
    }

    @GetMapping("/{id}")
    public Order fetchByOrderId(@PathVariable("id") long orderId){
        return  this.orderService.fetchById(orderId);
    }

    @DeleteMapping("/{id}")
    public void deletByOrderId(@PathVariable("id") long orderId){
        this.orderService.deleteById(orderId);
    }
}