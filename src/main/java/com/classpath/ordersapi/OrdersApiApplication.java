package com.classpath.ordersapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableWebSecurity
public class OrdersApiApplication{

    @Autowired
    private ApplicationContext applicationContext;

    public static void main(String[] args) {
        SpringApplication.run(OrdersApiApplication.class, args);
    }

    //@Override
    public void run(String... args) throws Exception {
        String[] beanDefinitionNames = this.applicationContext.getBeanDefinitionNames();

        for (String beanName: beanDefinitionNames){
            System.out.println(beanName);
        }
    }
}
