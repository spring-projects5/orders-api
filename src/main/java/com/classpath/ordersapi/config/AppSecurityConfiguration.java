package com.classpath.ordersapi.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import static org.springframework.http.HttpMethod.*;

@Configuration
@AllArgsConstructor
public class AppSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private UserDetailsService userDetailsService;

    @Override
    //setting up authentication
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {

        authenticationManagerBuilder
                .userDetailsService(userDetailsService)
                .passwordEncoder(NoOpPasswordEncoder.getInstance());
        /*authenticationManagerBuilder
                .inMemoryAuthentication()
                .withUser("kiran")
                .password("user123")
                .roles("USER")
                .and()
                .withUser("vinay")
                .password("admin123")
                .roles("USER", "ADMIN")
                .and()
                .passwordEncoder(NoOpPasswordEncoder.getInstance());*/
    }

    @Override
    //setting up authorization
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().disable()
            .csrf().disable().headers().frameOptions().disable()
            .and()
            .authorizeRequests()
                .antMatchers("/h2-console/**", "/login", "/logout").permitAll()
                .antMatchers(GET, "/api/v1/products/**").hasAnyRole("USER", "ADMIN")
                .antMatchers(GET, "/manage/**").hasAnyRole("USER", "ADMIN")
                .antMatchers(POST, "/api/v1/products/**").hasRole("ADMIN")
                .antMatchers(DELETE, "/api/v1/products/**").hasRole("ADMIN")
                .antMatchers(PUT, "/api/v1/products/**").hasRole("ADMIN")
                .antMatchers(POST, "/manage/**").hasRole("ADMIN")
            .anyRequest()
                .fullyAuthenticated()
            .and()
                .formLogin()
            .and()
                .httpBasic();
    }
}