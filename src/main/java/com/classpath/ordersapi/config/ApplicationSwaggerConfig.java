package com.classpath.ordersapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spring.web.plugins.Docket;

import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

@Configuration
public class ApplicationSwaggerConfig {

    @Bean
    public Docket swaggerApi(){
        return new Docket(SWAGGER_2)
                    .select()
                    .apis(RequestHandlerSelectors.basePackage("com.classpath.ordersapi"))
                    .paths(PathSelectors.any())
                    .build()
                    .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo() {
        return new ApiInfoBuilder()
                .title("Orders Rest API")
                .contact(new Contact("Pradeep", "http://classpath.io", "pradeep@classpath.io"))
                .description("A Simple API Documentation for exposing REST endpoints")
                .version("v1")
                .build();
    }
}