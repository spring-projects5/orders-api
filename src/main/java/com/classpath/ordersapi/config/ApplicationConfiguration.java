package com.classpath.ordersapi.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnJava;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = "app", name = "loadUser", havingValue = "true", matchIfMissing = false)
    public User user (){
        return new User();
    }
    @Bean
    @ConditionalOnMissingBean(name="user")
    public User userBean (){
        return new User();
    }
    @Bean
    @ConditionalOnJava(JavaVersion.EIGHT)
    public User userBeanOnJava8 (){
        return new User();
    }
    @Bean
    @ConditionalOnJava(JavaVersion.NINE)
    public User userBeanOnJava9 (){
        return new User();
    }

}

class User {
    private long id;
    private String name;
}