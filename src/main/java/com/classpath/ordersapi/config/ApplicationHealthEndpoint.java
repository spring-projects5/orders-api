package com.classpath.ordersapi.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import static org.springframework.boot.actuate.health.Status.UP;

@Component
public class ApplicationHealthEndpoint implements HealthIndicator {
    @Override
    public Health health() {
        // you define what is the definition of your application is ready to take request
        return Health.status(UP).withDetail("Payment Gateway Service ", "Payment Gateway Service is healthy").build();
    }
}


@Component
class MessageBrokerHealthEndpoint implements HealthIndicator {
    @Override
    public Health health() {
        // you define what is the definition of your application is ready to take request
        return Health.status(UP).withDetail("Message Broker ", "Message broker Service is healthy").build();
    }
}