package com.classpath.ordersapi.model;

import lombok.*;

import javax.persistence.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@ToString
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@Builder
@Entity
@AllArgsConstructor
@Table(name="orders")
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Getter
    private long id;

    @Setter
    @Getter
    private String customerName;

    @Setter
    @Getter
    private double orderPrice;

    @Setter
    @Getter
    private LocalDate orderDate;

    @Getter
    @OneToMany(mappedBy = "order", fetch = EAGER, cascade = ALL)
    private Set<OrderLineItem> orderLineItems = new HashSet<>();

    //scaffolding code to build the links in both the ways
    public void addLineItem(OrderLineItem lineItem){
        this.orderLineItems.add(lineItem);
        lineItem.setOrder(this);
    }

}