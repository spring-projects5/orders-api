package com.classpath.ordersapi.model;

import lombok.*;

import javax.persistence.*;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;


@EqualsAndHashCode(of = "id")
@NoArgsConstructor
@Builder
@Entity
@Table(name="order_line_items")
@AllArgsConstructor
public class OrderLineItem {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long lineItemId;

    @Getter
    @Setter
    private int qty;

    @Setter
    @Getter
    private double price;

   /* @OneToOne(mappedBy = "orderLineItem", cascade = ALL, fetch = EAGER)
    @Setter
    private Product product;
    */
    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    @Setter
    private Order order;

    /*
    public void addProduct(Product product){
        this.product = product ;
        this.product.setOrderLineItem(this);
    }*/

}