package com.classpath.ordersapi.service;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService{

    private OrderRepository orderRepository;

    @Override
    public Order save(Order order) {
        return this.orderRepository.save(order);
    }

    @Override
    public Set<Order> fetchAll() {
        return new HashSet<>(this.orderRepository.findAll());
    }

    @Override
    public Order fetchById(Long orderId) {
        return this.orderRepository
                    .findById(orderId).orElseThrow(() -> new IllegalArgumentException("Invalid Order Id"));
    }

    @Override
    public void deleteById(Long orderId) {
        this.orderRepository.deleteById(orderId);
    }
}