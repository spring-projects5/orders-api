package com.classpath.ordersapi.service;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.model.Product;

import java.util.Set;

public interface OrderService {

     Order save(Order order);

    Set<Order> fetchAll();

    Order fetchById(Long type);

    void deleteById(Long type);
}