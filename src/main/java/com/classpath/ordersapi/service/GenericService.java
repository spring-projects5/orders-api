package com.classpath.ordersapi.service;

import com.classpath.ordersapi.model.Product;

import java.util.Set;

public interface GenericService<E, T> {

    E save (E e);

    Set<E> fetchAll();

    Product fetchById(T type);

    void deleteById(T type);
}