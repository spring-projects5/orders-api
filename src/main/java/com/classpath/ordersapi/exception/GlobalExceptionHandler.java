package com.classpath.ordersapi.exception;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Error> handleRuntimeException(IllegalArgumentException exception){
        log.error("Exception caught {}", exception.getMessage());
        return  ResponseEntity.status(NOT_FOUND).body(Error.builder().errorCode(112).message(exception.getMessage()).build());
    }
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Error> handleConstraintViolationException(MethodArgumentNotValidException exception){
        log.error("Exception caught {}", exception.getMessage());
        return  ResponseEntity.status(BAD_REQUEST).body(Error.builder().errorCode(113).message(exception.getMessage()).build());
    }
}

@AllArgsConstructor
@ToString
@Builder
@Setter
@Getter
class Error {
    private long errorCode;
    private String message;
}